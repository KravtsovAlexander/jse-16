package ru.t1.kravtsov.tm.api.repository;

import ru.t1.kravtsov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User findByLogin(String login);

    User findByEmail(String email);

    Boolean doesLoginExist(String login);

    Boolean doesEmailExist(String email);

}
