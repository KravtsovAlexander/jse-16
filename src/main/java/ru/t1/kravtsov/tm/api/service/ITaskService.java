package ru.t1.kravtsov.tm.api.service;

import ru.t1.kravtsov.tm.api.repository.ITaskRepository;
import ru.t1.kravtsov.tm.enumerated.Status;
import ru.t1.kravtsov.tm.model.Task;

public interface ITaskService extends IService<Task>, ITaskRepository {

    Task create(String name);

    Task create(String name, String description);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

}
