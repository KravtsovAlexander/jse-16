package ru.t1.kravtsov.tm.api.repository;

import ru.t1.kravtsov.tm.model.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    List<Project> removeByName(String name);

}
