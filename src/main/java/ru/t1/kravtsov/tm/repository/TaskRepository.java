package ru.t1.kravtsov.tm.repository;

import ru.t1.kravtsov.tm.api.repository.ITaskRepository;
import ru.t1.kravtsov.tm.model.Task;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    public TaskRepository() {
    }

    @Override
    public List<Task> removeByName(final String name) {
        final List<Task> removedTasks = new ArrayList<>();
        final Iterator<Task> iterator = findAll().iterator();
        while (iterator.hasNext()) {
            final Task task = iterator.next();
            if (name.equals(task.getName())) {
                iterator.remove();
                removedTasks.add(task);
            }
        }

        return removedTasks;
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : models) {
            if (task.getProjectId() == null) continue;
            if (task.getProjectId().equals(projectId)) {
                result.add(task);
            }
        }
        return result;
    }

}
