package ru.t1.kravtsov.tm.command.user;

import ru.t1.kravtsov.tm.util.TerminalUtil;

public class UserChangePasswordCommand extends AbstractUserCommand {

    public static final String DESCRIPTION = "Change password of current user.";

    public static final String NAME = "user-change-password";

    @Override
    public void execute() {
        final String userId = getAuthService().getUserId();
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("NEW PASSWORD:");
        final String password = TerminalUtil.nextLine();
        getUserService().setPassword(userId, password);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
